import java.util.Collections;
import java.util.EnumSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * This is the functional approach for the same problem
 */
public class AppFunctional {

    public enum theNumberSet {

        M(1000),
        CM(900),
        D(500),
        CD(400),
        C(100),
        XC(90),
        L(50),
        XL(40),
        X(10),
        IX(9),
        V(5),
        IV(4),
        I(1);

        private final int numericValue;

        theNumberSet(int numericValue) {
            this.numericValue = numericValue;
        }

        // making the set an immutable set of collection.
        private static Set<theNumberSet> data = Collections
                .unmodifiableSet(EnumSet.allOf(theNumberSet.class));

        private static theNumberSet getTheNumber(int num){
            return data.stream().filter(n -> num >= n.numericValue).findFirst().orElse(I);
        }
    }

    public static String ConvertToRoman( int value) {
        return IntStream
                .iterate(value, element -> element -theNumberSet.getTheNumber(element).numericValue)
                .limit(theNumberSet.values().length)
                .filter(e -> e > 0)
                .mapToObj(theNumberSet::getTheNumber)
                .map(String::valueOf)
                .collect(Collectors.joining());
    }

    public static void main(String[] args) {
        System.out.println(ConvertToRoman(99));

    }
}
