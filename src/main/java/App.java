public class App {

    private static String[] romanSymbol = new String []{"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};

    private static int[] numbers = new int[]{ 1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};

    public static String convertIntegerToRoman(int value) {

        StringBuilder collectRoman = new StringBuilder();
        int index = 0;
        while (value > 0) {
            if (value - numbers[index] >= 0) {
                collectRoman.append(romanSymbol[index]);
                value = value- numbers[index];
            }
            else {
                index++;
            }
        }
        return collectRoman.toString();
    }

    public static void main(String[] args) {

        System.out.println(convertIntegerToRoman(99));

    }
}
