import org.junit.Test;

import static org.junit.Assert.*;

public class AppFunctionalTest {

    @Test
    public void encodeTheNumber() {
        String actual = AppFunctional.ConvertToRoman(999);
        assertEquals("CMXCIX",actual);
    }
}