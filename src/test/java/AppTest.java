import org.junit.Test;

import static org.junit.Assert.*;

public class AppTest {

    @Test
    public void convertIntegerToRoman() {

        String check = App.convertIntegerToRoman(99);
        assertEquals("XCIX", check);
    }
}