# Coding Kata

The purpose of this exercise is to convert the any Numeric number to Roman Number.

The following points has been analyzed.

## What I have learned ?

I have learned about the structure of Roman numbers and the appropriate algorithm for it.

##### The structure of Roman number:

The main Roman characters are as follows
 
 |Symbol|Value|
 |------|-----|
 |I|     1| 
 |V|     5| 
 |X|     10| 
 |L|     50|
 |C|     100|
 |D|     500|
 |M|     1000| 
 
 
for conversion: putting a symbol at the left  of a symbol means subtraction and at the right is addition.
However we can add maximum 3 digit. 

For example if we want to convert 8, it will be **VIII** . 

5 -> V and 1 -> I. Therefore ***8 = 5+1+1+1 = VIII*** 

but we want to convert 9, we can not add another number, so the closest point to 9 is 10 and there is a symbol for it.

so we can subtract 1 from 10 . Therefore ***9 = 10 - 1 = IX***

When we have 10ths or 100th or 1000th, we separate it first.

Now if we need to convert 305, we can break it down as follows:

* 305 = 300 + 5
* 305 = 100+100+100+ 5 
*  Conversion:
    * 305 = CCCV

Now if we want to convert 915, lets break it down first

* 915 = 900 + 10 + 5
* 915 = (1000-100) + 10 + 5
* 915 = CMXV

As you can see for 900 we can not make it DCCCC .However can subtract 100 from 1000 , which will not violate the conversion rule.

### The Algorithm

* First of all, we have to setup our arrays of Roman symbols and corresponding numerics.

* If we want to convert a value and the difference between the value and first indexed value of the number array is not bigger or equal to zero, then we will increase the index and check again. Once it's matched then we will collect the roman symbol from that index.

* After that, we will reduce the value which is the difference between the given value and the founded indexed number. 
this process will continue how long the given number is bigger than zero. 

**Notice:** This algorithm will only work if the given array formation is as it is done in the code ( App.java )

## What I will do differently next time ?

I will do the following things next time:

* Creating a TreeMap of set of Roman-number combination and iterating over a treeMap to perform the algorithm and  collect value.
    * Why TreeMap ? 
        * Because TreeMap iterates according to the natural ordering of keys.
         
    _[ However I Think Enum is more convinent for this type of problem, which is also implemented in AppFunctional.java ]_
    
## What part of my solution am I especially proud of ?

Begin able to iterate over an array not with multiple if clause.

However initially I came up with such solution but some information roman symbol construction helped  to re think.



